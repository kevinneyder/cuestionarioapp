const question = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName("choice-text"));
const progressText = document.getElementById("progressText");
const scoreText = document.getElementById("score");
const progressBarFull = document.getElementById("progressBarFull");
const loader = document.getElementById("loader");
const game = document.getElementById("game");
let currentQuestion = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuesions = [];


var questions = [
  {
    "question": "Â¿Que es html?",
    "choice1": "Lenguaje de enmarcado",
    "choice2": "Lenguaje de programacion",
    "choice3": "IDE",
    "choice4": "Ninguno",
    "answer": 1
  },
  {
    "question": "Cual es la opcion correcta de un hola mundo en java",
    "choice1": "\"Hola Mundo\"",
    "choice2": "print(\"Hola Mundo\")",
    "choice3": "System.out.print(\"Hola Mundo\")",
    "choice4": "show(hola mundo)",
    "answer": 3
  },
  {
    "question": "Cual es la opcion correcta de un alert en javascript",
    "choice1": "msgBox('Hello World');",
    "choice2": "alertBox('Hello World');",
    "choice3": "msg('Hello World');",
    "choice4": "alert('Hello World');",
    "answer": 4
  },
  {
    "question": "Etiqueta para introducir codigo JavaScript??",
    "choice1": "<script>",
    "choice2": "<javascript>",
    "choice3": "<js>",
    "choice4": "<scripting>",
    "answer": 1
  },
  {
    "question": "Cual es la forma correcta de hacer referencia un archivo javascript 'xxx.js'?",
    "choice1": "<script href='xxx.js'>",
    "choice2": "<script name='xxx.js'>",
    "choice3": "<script src='xxx.js'>",
    "choice4": "<script file='xxx.js'>",
    "answer": 3
  },
  {
    "question": "Python es:",
    "choice1": "Un comando linux",
    "choice2": "Tipo de dato",
    "choice3": "Un editor de codigo",
    "choice4": "Lenguaje de programacion",
    "answer": 4
  },
  {
    "question": "Cual de estos NO es un lenguaje de programacion",
    "choice1": "Java",
    "choice2": "HTML",
    "choice3": "Php",
    "choice4": "C#",
    "answer": 2
  }
]

//CONSTANTS
const CORRECT_BONUS = 10;
const MAX_QUESTIONS = 7;
startGame();
function startGame (){
  console.log(questions);
  questionCounter = 0;
  score = 0;
  availableQuesions = [...questions];
  getNewQuestion();
  game.classList.remove("hidden");
  loader.classList.add("hidden");
};

function getNewQuestion(){
  if (availableQuesions.length === 0 || questionCounter >= MAX_QUESTIONS) {    
    localStorage.setItem("mostRecentScore", score);
    //ir html fin del juego
    return window.location.assign("./fin.html");
  }
  move();
  //Barra de preguntas
  questionCounter++;
  progressText.innerText = `Preguntas ${questionCounter}/${MAX_QUESTIONS}`;
  progressBarFull.style.width = `${(questionCounter / MAX_QUESTIONS) * 100}%`;
  
  const questionIndex = Math.floor(Math.random() * availableQuesions.length);
  currentQuestion = availableQuesions[questionIndex];
  question.innerText = currentQuestion.question;

  choices.forEach(choice => {
    const number = choice.dataset["number"];
    choice.innerText = currentQuestion["choice" + number];
  });

  availableQuesions.splice(questionIndex, 1);
  acceptingAnswers = true;
};

choices.forEach(choice => {
  choice.addEventListener("click", e => {
    if (!acceptingAnswers) return;

    acceptingAnswers = false;
    const selectedChoice = e.target;
    const selectedAnswer = selectedChoice.dataset["number"];

    const classToApply =
      selectedAnswer == currentQuestion.answer ? "correct" : "incorrect";

    if (classToApply === "correct") {
      playSucessful();
      incrementScore(CORRECT_BONUS);
    }else{
      playError();
    }

    selectedChoice.parentElement.classList.add(classToApply);

    setTimeout(() => {
      selectedChoice.parentElement.classList.remove(classToApply);
      getNewQuestion();
    }, 1000);
  });
});

incrementScore = num => {
  score += num;
  scoreText.innerText = score;
};

function playSucessful(){
  
  var audio = document.getElementById("audio1");
  audio.play();
  Swal.fire({
    icon: 'success',
    title: 'Bien!!!...',
    text: 'Buen trabajo!'
  })
}

function playError(){
  var audio = document.getElementById("audio2");
  audio.play();
  Swal.fire({
    icon: 'error',
    title: 'Oopsss!...',
    text: 'Respuesta incorrecta!'
  })
  
}

function playAlarm(){
  var audio = document.getElementById("audio3");
  audio.play();
  Swal.fire({
    icon: 'warning',
    title: 'Oopsss!...',
    text: 'Se agoto su tiempo!'
  })
  
}


function move() {
  var i;
  i = 0;
  if (i == 0) {
    i = 1;
    var elem = document.getElementById("myBar");
    var width = 1;
    var id = setInterval(frame, 50);
    function frame() {
      if (elem.style.width == "100%") {
        //alert("Tiempo terminado");
        playAlarm();
        getNewQuestion();
        clearInterval(id);
        elem.style.width = 0+"%";
        i = 0;
      } else {
        width++;
        elem.style.width = width + "%";
      }
    }
  }
}

